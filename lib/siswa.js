const fs = require('fs');

const siswa = (query) => new Promise((resolve, reject) => {
    console.log(`Search for siswa ${query}`)
    // get datas from ../database/siakad/x/ where query is similar file name in directory and split by underscore and make toUpperCase
    let datas = JSON.parse(fs.readFileSync("./database/siswa/" + query.split(" ").join("_").toUpperCase() + ".json")) // get all data in file    
    // check if datas is not empty
    if (datas.length !== 0) {
        // resolve datas
        let result = {
            status: "success",
            code: 200,
            creator: "Nabils24",
            message: "Data ditemukan",
            data: {
                nama: datas.profile_sekolah.dasar.nama,
                nisn: datas.profile_sekolah.murid.nisn,
                tempat_lahir: datas.profile_sekolah.dasar.lahir,
                jenis_kelamin: datas.profile_sekolah.dasar.kelamin,
                alamat: datas.profile_sekolah.murid.alamat,
                foto_siakad: datas.profile_sekolah.dasar.foto_siakad,
            }
        }
        resolve(result)
    } else {
        // reject with error message
        reject("Data tidak ditemukan")
    }
})

exports.siswa = siswa
