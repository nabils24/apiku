const qrcode = require("qrcode-terminal");
const fs = require("fs");
const { Client, LocalAuth, NoAuth, ClientInfo } = require("whatsapp-web.js");

// Use the saved values
const client = new Client({
  authStrategy: new LocalAuth(),
});

client.on("qr", (qr) => {
  qrcode.generate(qr, { small: true });
});

client.on("ready", () => {
  console.log("Client is ready!");
});

client.initialize();

const sendMessage = (number, message) =>
  new Promise((resolve, reject) => {
    var numberku = number + "@c.us";

    client
      .sendMessage(numberku, message)
      .then((response) => {
        resolve({
            code: 200,
            status: "success",
            message: "Message sent!",
        });
      })
      .catch(() => {
        resolve({
            code: 400,
            status: "error",
            message: "Message not sent!",
        });
      });
  });





module.exports = {
  sendMessage,
}

