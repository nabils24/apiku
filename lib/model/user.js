// Variable Deklarasi
__path = process.cwd();
var userdb = require("../controller/user");
var express = require("express");
var request = require("request");
var secure = require("ssl-express-www");
var cors = require("cors");
var router = express.Router();

router.post("/register", async (req, res, next) => {
  var data = {
    nama: req.query.nama,
    password: req.query.password,
    email: req.query.email,
    nohp: req.query.nohp,
    createtime: (timestamp = new Date().toLocaleString("id-ID")),
    editetime: (timestamp = new Date().toLocaleString("id-ID")),
  };
  if (!data.nama || !data.password || !data.email || !data.nohp)
    return res.status(406).json({
      status: "error",
      message: "Salah Satu Query Tidak Boleh Kosong!",
    });
  userdb
    .adduser(
      data.nama,
      data.password,
      data.email,
      data.nohp,
      data.createtime,
      data.editetime
    )
    .then((result) => {
      res.status(200).json({
        status: "success",
        message: "Berhasil Menambahkan User!",
        result: result,
      });
    });
});

router.post("/login", async (req, res, next) => {
  var data = {
    nama: req.query.nama,
    password: req.query.password,
  };
  if (!data.nama || !data.password )
    return res.status(406).json({
      status: "error",
      message: "Salah Satu Query Tidak Boleh Kosong!",
    });
    userdb.showuser(data.nama, data.password).then((result) => {
        res.status(200).json({result});
    }); 
});

router.post("/edit", async (req, res, next) => {
    var data = {
        nama: req.query.nama,
        password: req.query.password,
        email: req.query.email,
        nohp: req.query.nohp,
        editetime: (timestamp = new Date().toLocaleString("id-ID")),
    };
    if (!data.nama || !data.password || !data.email || !data.nohp)
        return res.status(406).json({
            status: "error",
            message: "Salah Satu Query Tidak Boleh Kosong!",
        });
    userdb.edituser(data.nama, data.password, data.email, data.nohp, data.editetime).then((result) => {
        res.status(200).json({result});
    });
});

router.post("/delete", async (req, res, next) => {
    var data = {
        nama: req.query.nama,
    };
    if (!data.nama)
        return res.status(406).json({
            status: "error",
            message: "Salah Satu Query Tidak Boleh Kosong!",
        });
    userdb.deleteuser(data.nama).then((result) => {
        res.status(200).json({result});
    });
});

router.use(function (req, res) {
    res
      .status(404)
      .set("Content-Type", "text/html")
      .sendFile(__path + "/views/404.html");
  });
module.exports = router;
