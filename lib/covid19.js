const axios = require("axios");

const covid19 = (negara) => new Promise((resolve, reject) => {
    const options = {
        method: 'GET',
        url: 'https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/total',
        params: { country: negara },
        headers: {
            'X-RapidAPI-Key': '15a4661575msh3a385e1a08d3b95p112281jsn6214caa45417',
            'X-RapidAPI-Host': 'covid-19-coronavirus-statistics.p.rapidapi.com'
        }
    };

    axios.request(options).then(function (response) {
        let result = {
            status: "success",
            code: 200,
            creator: "Nabils24",
            message: "Covid Di Negara " + negara,
            data: {
                negara: response.data.data.location,
                kasus: response.data.data.confirmed,
                meninggal: response.data.data.deaths,
                sembuh: response.data.data.recovered,
            }
        }
        resolve(result)
    }).catch(function (error) {
        let result = {
            status: "error",
            code: 404,
            creator: "Nabils24",
            message: "Covid Di Negara " + negara,
            data: []
        }
        resolve(result)
    });
})

exports.covid19 = covid19
