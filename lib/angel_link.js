const fs = require('fs-extra')
const randomAvatar = require('random-avatar')

const adduser = (ig, time) =>
  new Promise((resolve, reject) => {
    // create file json in ./database/user and file name is name params
    const obj = {
      ig: ig,
      time: time,
      msg: []
    }
    fs.writeFileSync('./database/user/' + ig + '.json', JSON.stringify(obj))
    const result = {
      status: 'success',
      code: 200,
      creator: 'Nabils24',
      message: 'Success add user ' + ig,
      data: obj
    }
    resolve(result)
  })

const buatpesan = (serialkey, img, to, msg, time, userData) =>
  new Promise((resolve, reject) => {
    // update key msg from file json in ./database/user/ and file name is to params
    fs.readFile('./database/user/' + to + '.json', (err, data) => {
      if (err) {
        const result = {
          status: 'error',
          code: 404,
          creator: 'Nabils24',
          message: 'Error read file'
        }
        resolve(result)
      } else {
        const obj = JSON.parse(data)
        obj.msg.push({
          serialkey: serialkey,
          img: img,
          msg: msg,
          time: time,
          userdata: userData
        })
        fs.writeFileSync('./database/user/' + to + '.json', JSON.stringify(obj))
        const result = {
          status: 'success',
          code: 200,
          creator: 'Nabils24',
          message: 'Success add msg to ' + to,
          data: obj
        }
        resolve(result)
      }
    })
  })

const showallmsgfrom = to =>
  new Promise((resolve, reject) => {
    // read file json in ./database/user/ and file name is to params
    fs.readFile('./database/user/' + to + '.json', (err, data) => {
      if (err) {
        const result = {
          status: 'error',
          code: 404,
          creator: 'Nabils24',
          message: 'Error read file',
        }
        resolve(result)
      } else {
        const obj = JSON.parse(data)
        const result = {
          status: 'success',
          code: 200,
          creator: 'Nabils24',
          message: 'Success read msg from ' + to,
          data: obj.msg
        }
        resolve(result)
      }
    })
  })

module.exports = {
  adduser,
  buatpesan,
  showallmsgfrom
}
