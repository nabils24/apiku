const fs = require("fs-extra");
var _dir= JSON.parse(
    fs.readFileSync("./database/listkey.json")
);
/**
 * Add user to database.
 * @param {string} namekey
 * @param {string} limit
 */
const addRegisteredkey = (namekey, limit) => {
    const obj = { namekey: namekey, limit: limit }
    _dir.push(obj)
    fs.writeFileSync('./database/listkey.json', JSON.stringify(_dir))
}
const cekRegisteredkey = (namekey) => {
    let status = false
    Object.keys(_dir).forEach((i) => {
        if (_dir[i].namekey === namekey) {
            status = true
        }
    })
    return status
}

// make const reduce limit apikey
const reduceLimit = (namekey) => {
    let position = false
    Object.keys(_dir).forEach((i) => {
        if (_dir[i].namekey === namekey) {
            position = i
        }
    })
    if (position !== false) {
        _dir[position].limit -= 1
        fs.writeFileSync('./database/listkey.json', JSON.stringify(_dir))
    }
}

const showallregisteredkey = () => {
    return _dir
}
    

module.exports = {
    addRegisteredkey,
    cekRegisteredkey,
    showallregisteredkey,
    reduceLimit
}
