module.exports = {
	Searchnabi: require('./utils/kisahnabi'),
	Gempa: require('./utils/gempa'),
	apikey: require('./apikey'),
	siswa: require('./siswa'),
	covidku: require('./covid19'),
	filmku: require('./film'),
	cnn: require('./cnn'),
	crud: require('./angel_link'),
	fakeimgface: require('./fakeimgface'),
	mongodb: require('./crud'),
	otp: require('./otpigs'),
	waApi: require('./whatsApi/whatsapp'),

}
