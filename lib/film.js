const axios = require('axios')
const cheerio = require('cheerio')

const film = () => new Promise((resolve, reject) => {
  try {
    const url = 'https://lk21official.info/top-movie-today/'
    axios.get(url)
      .then((response) => {
        if (response.status === 200) {
          const html = response.data
          const $ = cheerio.load(html)
          const film = []
          $('#grid-wrapper div.col-lg-2').each(function (i, elem) {
            film.push({
              title: $(this).find('a').attr('title'),
              link: $(this).find('a').attr('href'),
              img: "https:" + $(this).find('img').attr('src'),
              duration: $(this).find('.duration').text(),
              kualitas: $(this).find('.quality').text(),
              rating: $(this).find('.rating').text()
            })
          });
          const result = {
            status: 'success',
            code: 200,
            creator: 'Nabils24',
            message: 'Film Terbaru By LayarKaca21',
            data: film
          }
          resolve(result)
        }
      })
  } catch (e) {
    reject(e)
  }
})

exports.filmku = film
