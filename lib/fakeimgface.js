const url = "https://fakeface.rest/face/json";
const axios = require("axios");

const generate = () =>
  axios
    .get(url)
    .then((response) => {
      console.log(response.data);
        return response.data;
    })
    .catch((error) => {
      console.log(error);
    });

module.exports = { generate };
