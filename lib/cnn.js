const axios = require('axios')
const cheerio = require('cheerio')

const cnn = () => new Promise((resolve, reject) => {
    try {
        const url = 'https://www.cnnindonesia.com/indeks'
        axios.get(url)
            .then((response) => {
                if (response.status === 200) {
                    const html = response.data
                    const $ = cheerio.load(html)
                    const cnn = []
                    $('.list article a').each(function (i, elem) {
                        cnn.push({
                            title: $(this).find('span h2').text(),
                            img: $(this).find('img').attr('src'),
                            link: $(this).attr('href'),

                        })
                    });
                    const result = {
                        status: 'success',
                        code: 200,
                        creator: 'Nabils24',
                        message: 'CNN Indonesia',
                        data: cnn
                    }
                    resolve(result)
                }
            })
    } catch (e) {
        const result = {
            status: 'error',
            code: 404,
            creator: 'Nabils24',
            message: 'CNN Indonesia',
            data: []
        }
        resolve(result)
    }
})
const cnncheckq = (q) => new Promise((resolve, reject) => {
    try {
        const url = q
        axios.get(url)
            .then((response) => {
                if (response.status === 200) {
                    const html = response.data
                    const $ = cheerio.load(html)
                    const cnn = []
                    cnn.push({
                        title: $('div.content_detail').find('h1.title').text().trim(),
                        date: $('div.content_detail').find('div.date').text(),
                        img: $('div.content_detail').find('div.media_artikel img').attr('src'),
                        desc: $('div.content_detail').find('div.detail_wrap div.detail_text p').text().split('\n').join('').split('"ADVERTISEMENTSCROLL TO RESUME CONTENT"').join('').split('[Gambas:Video CNN]').join('').trim(),
                    })

                    const result = {
                        status: 'success',
                        code: 200,
                        creator: 'Nabils24',
                        message: 'CNN Indonesia By Link',
                        data: cnn
                    }
                    resolve(result)
                }
            })
    } catch (e) {
        const result = {
            status: 'error',
            code: 404,
            creator: 'Nabils24',
            message: 'CNN Indonesia By Link',
            data: []
        }
        resolve(result)
        
    }
})
module.exports = {
    cnn,
    cnncheckq
}