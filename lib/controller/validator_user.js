const user = require('./user')
const creator = 'celenganku'

const errorhandler = {
        noturl: {
          status: false,
          creator: `${creator}`,
          code: 404,
          message: "masukan parameter url",
        },
        notusername: {
          status: false,
          creator: `${creator}`,
          code: 404,
          message: "masukan parameter username",
        },
        notvalue: {
          status: false,
          creator: `${creator}`,
          code: 404,
          message: "masukan parameter value",
        },
        invalidKey: {
          status: false,
          creator: `${creator}`,
          code: 406,
          message: "apikey invalid",
        },
        invalidlink: {
          status: false,
          creator: `${creator}`,
          message: "error, mungkin link anda tidak valid.",
        },
        invalidkata: {
          status: false,
          creator: `${creator}`,
          message: "error, mungkin kata tidak ada dalam api.",
        },
        error: {
          status: false,
          creator: `${creator}`,
          message: "404 ERROR",
        },

}

module.exports = {
    errorhandler
}