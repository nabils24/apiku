var mongodb = require("mongodb");
var MongoClient = mongodb.MongoClient;
var dotenv = require("dotenv");
var bcrypt = require("bcrypt");

var url = `mongodb+srv://${process.env.USERNAME_MONGODB}:${process.env.PASSWORD_MONGODB}@apiku-db.kfe6tej.mongodb.net/?retryWrites=true&w=majority`;

const adduser = (nama, password, email, nohp, createtime, editetime) => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      function (err, db) {
        if (err) throw err;
        var dbo = db.db("apiku");

        var myobj = {
          nama: nama,
          password: bcrypt.hashSync(password, 10),
          email: email,
          nohp: nohp,
          createtime: createtime,
          editetime: editetime,
        };
        dbo.collection("user").insertOne(myobj, function (err, res) {
          if (err) throw err;
          if (err) reject("error add user");
          resolve("success add user " + res.insertedId);
          db.close();
        });
      }
    );
  });
};

const edituser = (nama, password, email, nohp, editetime) => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      function (err, db) {
        if (err) throw err;
        var dbo = db.db("apiku");
        var myobj = {
          nama: nama,
          password: bcrypt.hashSync(password, 10),
          email: email,
          nohp: nohp,
          editetime: editetime,
        };
        dbo
          .collection("user")
          .updateOne({ nama: nama }, { $set: myobj }, function (err, result) {
            if (err) {
                reject(err);
              } else {
                  resolve({
                    status: "success",
                    message: "Berhasil mengubah data user",
                  });
                  
              }
              db.close();
           
          });
      }
    );
  });
};

const deleteuser = (nama) => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      function (err, db) {
        if (err) throw err;
        var dbo = db.db("apiku");
        dbo.collection("user").deleteOne({ nama: nama }, function (err, obj) {
          if (err) throw err;
          return "success delete user";
          db.close();
        });
      }
    );
  });
};

const showuser = (nama, password) => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      function (err, db) {
        if (err) throw err;
        var dbo = db.db("apiku");
        dbo.collection("user").findOne({ nama: nama }, (err, result) => {
          const isMatch = bcrypt.compareSync(password, result.password);
          if (err) {
            reject(err);
          } else {
            if (isMatch) {
              resolve({
                status: "success",
                data: result,
              });
            } else {
              resolve({
                status: "success",
                message: "Password Salah",
              });
            }
            db.close();
          }
        });
      }
    );
  });
};

const showalluser = () => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      function (err, db) {
        if (err) throw err;
        var dbo = db.db("apiku");
        dbo
          .collection("user")
          .find({})
          .toArray(function (err, result) {
            if (err) throw err;
            return result;
            db.close();
          });
      }
    );
  });
};

module.exports = {
  adduser,
  edituser,
  deleteuser,
  showuser,
  showalluser,
};
