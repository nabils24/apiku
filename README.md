# Apiku

Simple Rest Api From Express JS


## Pembuat

- [@nabils24](https://www.github.com/nabils24)



## Instal

Install my-project with npm

```bash
  git clone https://gitlab.com/nabils24/apiku.git
  cd apiku
  npm i
  npm run start
```

## Screenshots

Apiku
![App Screenshot](https://telegra.ph/file/d810c6af33d437a1d5f8c.png)

## Fitur

- Gempa Terkini Scrape > BMKG
- LayarKaca21 Scrape > LayarKaca21
- CNN News > CNN 
- Covid > GET > rapiapi


## license

[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)
[![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)



## Feedback

Jika ada bug atau mau kasih saran bisa hubungi email saya nabilsahsadacode@gmail.com

Oh, iya jangan lupa starnya yaaaa🌟🌟

Makasihhh
