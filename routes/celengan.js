__path = process.cwd();
//var favicon = require('serve-favicon');
var express = require("express");
var creator = "celengan";
var secure = require("ssl-express-www");
var cors = require("cors");
var fetch = require("node-fetch");
var cheerio = require("cheerio");
var request = require("request");
var axios = require("axios");
var zrapi = require("zrapi");
var dotenv = require("dotenv").config();
var fs = require("fs");
var router = express.Router();
var serial = require("generate-serial-key");
var avatars = require("give-me-an-avatar");
var randomAvatar = require("random-avatar");



var {
  user,
} = require("../lib/model");



var cookie = process.env.COOCKIE;



// set header api
router.use(function (req, res, next) {
  res.setHeader('X-Powered-By', 'Celengan')
  res.setHeader('contact', 'nabilsahsadacode@gmail.com')
  next()
})

router.use("/user", user);

router.get("/", async (req, res, next) => {
  res.status(200).json({
    code:200,
    status: "success",
    message: "Selamat Datang Di Celengan API | Siap",
    
  });
});

router.use(function (req, res) {
  res
    .status(404)
    .set("Content-Type", "text/html")
    .sendFile(__path + "/views/404.html");
});

module.exports = router;
