__path = process.cwd();
//var favicon = require('serve-favicon');
var express = require("express");

var creator = "Nabils24";
var secure = require("ssl-express-www");
var cors = require("cors");
var fetch = require("node-fetch");
var cheerio = require("cheerio");
var request = require("request");
var axios = require("axios");
var zrapi = require("zrapi");
var dotenv = require("dotenv").config();
var fs = require("fs");
var TikTokScraper = require("tiktok-scraper");
var { EmojiAPI } = require("emoji-api");
var emoji = new EmojiAPI();
var router = express.Router();
var { TiktokDownloader } = require("../lib/tiktokdl.js");
var { color, bgcolor } = require(__path + "/lib/color.js");
var { fetchJson } = require(__path + "/lib/fetcher.js");
var options = require(__path + "/lib/options.js");
var serial = require("generate-serial-key");
var avatars = require("give-me-an-avatar");
var randomAvatar = require("random-avatar");

var {
  pShadow,
  pRomantic,
  pSmoke,
  pBurnPapper,
  pNaruto,
  pLoveMsg,
  pMsgGrass,
  pGlitch,
  pDoubleHeart,
  pCoffeCup,
  pLoveText,
  pButterfly,
} = require("./../lib/utils/photooxy");

var {
  ttdownloader,
  pinterest,
  fbdown,
  igstalk,
  igstory,
  igdl,
  linkwa,
  igDownloader,
} = require("./../lib/anjay");

var { igStalk, igDownloader } = require("./../lib/utils/igdown");

var {
  ytDonlodMp3,
  ytDonlodMp4,
  ytPlayMp3,
  ytPlayMp4,
  ytSearch,
} = require("./../lib/utils/yt");

var { Joox, FB, Tiktok } = require("./../lib/utils/downloader");

var { Cuaca, Lirik } = require("./../lib/utils/information");

var { fbDownloader, fbdown2 } = require("./../lib/utils/fbdl");

////////////////////////////////////////////////

var {
  Searchnabi,
  Gempa,
  apikey,
  siswa,
  covidku,
  filmku,
  cnn,
  crud,
  fakeimgface,
  mongodb,
  otp,
  waApi,
} = require("./../lib");
const { Router } = require("express");

var cookie = process.env.COOCKIE;

/*
 * @Pesan Error
 */
loghandler = {
  notparam: {
    status: false,
    creator: `${creator}`,
    code: 404,
    message: "masukan parameter apikey",
  },
  noturl: {
    status: false,
    creator: `${creator}`,
    code: 406,
    message: "masukan parameter url",
  },
  notusername: {
    status: false,
    creator: `${creator}`,
    code: 406,
    message: "masukan parameter username",
  },
  notvalue: {
    status: false,
    creator: `${creator}`,
    code: 406,
    message: "masukan parameter value",
  },
  invalidKey: {
    status: false,
    creator: `${creator}`,
    code: 406,
    message: "apikey invalid",
  },
  invalidlink: {
    status: false,
    creator: `${creator}`,
    message: "error, mungkin link anda tidak valid.",
  },
  invalidkata: {
    status: false,
    creator: `${creator}`,
    message: "error, mungkin kata tidak ada dalam api.",
  },
  error: {
    status: false,
    creator: `${creator}`,
    message: "404 ERROR",
  },
};

/*
Akhir Pesan Error
*/
// set header api
router.use(function (req, res, next) {
  res.setHeader('X-Powered-By', 'Nabils24-Api')
  res.setHeader('contact', 'nabilsahsadacode@gmail.com')
  next()
})

router.post("/apikey", async (req, res, next) => {
  const namekey = req.query.namekey;
  const limit = req.query.limit;
  if (apikey.cekRegisteredkey(namekey)) {
    res.json({
      message: "apikey sudah terdaftar",
    });
  } else {
    apikey.addRegisteredkey(namekey, limit);
    res.json({
      status: "success",
      code: 200,
      creator: `${creator}`,
      message: `berhasil mendaftarkan ${namekey} dengan ${limit} Kedatabase apikey`,
    });
  }
});

router.get("/cekapikey", async (req, res, next) => {
  const namekey = req.query.namekey;
  if (apikey.cekRegisteredkey(namekey)) {
    res.json({
      status: "success",
      code: 200,
      creator: `${creator}`,
      message: `apikey ${namekey} terdaftar`,
    });
  } else {
    res.status(404);
    res.json({
      status: "error",
      code: 404,
      creator: `${creator}`,
      message: `apikey ${namekey} tidak terdaftar`,
    });
  }
});

router.get("/listapikey", async (req, res, next) => {
  res.status(200).json({
    status: "success",
    code: 200,
    creator: `${creator}`,
    apikey: apikey.showallregisteredkey(),
  });
});

// apiku
router.get("/cekgempa", async (req, res, next) => {
  var Apikey = req.query.apikey;
  if (!Apikey) return res.json(loghandler.notparam);
  if (apikey.cekRegisteredkey(Apikey)) {
    Gempa().then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});

router.get("/carisiswamoklet", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var nama = req.query.nama;
  if (!Apikey) return res.json(loghandler.notparam);
  if (!nama) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    siswa.siswa(nama).then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});
router.get("/covid19", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var country = req.query.country;
  if (!Apikey) return res.json(loghandler.notparam);
  if (!country) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    covidku.covid19(country).then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});
router.get("/layarkaca21", async (req, res, next) => {
  var Apikey = req.query.apikey;
  if (!Apikey) return res.json(loghandler.notparam);
  if (apikey.cekRegisteredkey(Apikey)) {
    filmku.filmku().then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});

// INFOMATION
router.get("/cnnterbaru", async (req, res, next) => {
  var Apikey = req.query.apikey;
  if (!Apikey) return res.json(loghandler.notparam);
  if (apikey.cekRegisteredkey(Apikey)) {
    cnn.cnn().then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});
router.get("/liatberitacnn", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var url = req.query.url;
  if (!Apikey) return res.json(loghandler.notparam);
  if (!url) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    cnn.cnncheckq(url).then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});

// angel.link apis
router.get("/nglku/adduser", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var ig = req.query.ig;
  var timestamp = new Date().toLocaleString("id-ID");
  if (!Apikey) return res.json(loghandler.notparam);
  if (!ig) return res.json(loghandler.notvalue);

  if (apikey.cekRegisteredkey(Apikey)) {
    crud.adduser(ig, timestamp).then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});

router.get("/nglku/buatpesan", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var to = req.query.to;
  var pesan = req.query.pesan;
  var timestamp = new Date().toLocaleString("id-ID");
  var userData = {
    IpAddress: req.ip,
    browser: req.useragent.browser,
    version: req.useragent.version,
    os: req.useragent.os,
    platform: req.useragent.platform,
    source: req.useragent.source,
  };
  var serialku = serial.generate(20, "_", 5);
  var img =
    "https://png.pngtree.com/png-vector/20220608/ourmid/pngtree-user-mysterious-anonymous-account-vector-png-image_4816288.png";

  if (!Apikey) return res.json(loghandler.notparam);
  if (!to) return res.json(loghandler.notvalue);
  if (!pesan) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    crud
      .buatpesan(serialku, img, to, pesan, timestamp, userData)
      .then((result) => {
        apikey.reduceLimit(Apikey);
        if (result.code == 200) {
          res.status(result.code).json(result);
        } else {
          res.status(result.code).json(result);
        }
      });
  } else {
    res.json(loghandler.invalidKey);
  }
});

router.get("/nglku/listmsg", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var to = req.query.to;
  if (!Apikey) return res.json(loghandler.notparam);
  if (!to) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    crud.showallmsgfrom(to).then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});

// Generate OTP
router.get("/otp_ig", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var ig = req.query.ig;
  
  if (!Apikey) return res.json(loghandler.notparam);
  if (!ig) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    otp.kirimotp(ig).then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});

router.get("/otp_wa", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var no = req.query.no;
  var otp = serial.generate(5, "_", 5)

  var obj = "Haii, ini OTP kamu \n\n "+"*"+otp+"*"+'\n\n'+"Jangan berikan kode ini kepada siapapun termasuk kepada kami.'"
  if (!Apikey) return res.json(loghandler.notparam);
  if (!no) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    waApi.sendMessage(no, obj).then((result) => {
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});


// Whatsapp API
router.get("/wa/kirimpesan", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var wa = req.query.wa;
  var pesan = req.query.pesan;

  if (!Apikey) return res.json(loghandler.notparam);
  if (!wa) return res.json(loghandler.notvalue);
  if (!pesan) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    waApi.sendMessage(wa, pesan).then((result) => {
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});



    



// mongodb
router.get("/mongoku/listuser", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var col =  req.query.col;
  if (!Apikey) return res.json(loghandler.notparam);
  if (apikey.cekRegisteredkey(Apikey)) {
    mongodb.findCollection(col).then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});

router.get("/mongoku/adduser", async (req, res, next) => {
  var Apikey = req.query.apikey;
  var col =  req.query.col;
  var ig = req.query.ig;
  var timestamp = new Date().toLocaleString("id-ID");
  if (!Apikey) return res.json(loghandler.notparam);
  if (!ig) return res.json(loghandler.notvalue);
  if (apikey.cekRegisteredkey(Apikey)) {
    const obj = {
      ig: ig,
      time: timestamp,
      msg: []
    }
    mongodb.insertCollection(col, obj).then((result) => {
      apikey.reduceLimit(Apikey);
      if (result.code == 200) {
        res.status(result.code).json(result);
      } else {
        res.status(result.code).json(result);
      }
    });
  } else {
    res.json(loghandler.invalidKey);
  }
});
  


router.use(function (req, res) {
  res
    .status(404)
    .set("Content-Type", "text/html")
    .sendFile(__path + "/views/404.html");
});

module.exports = router;
