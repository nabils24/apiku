//yy
__path = process.cwd();
//var favicon = require('serve-favicon');
var express = require("express"),
  cors = require("cors"),
  secure = require("ssl-express-www");
const PORT = process.env.PORT || 8080 || 5000 || 3000;
var expressip = require("express-ip");
var bodyParser = require("body-parser");
var useragent = require("express-useragent");
var cookieParser = require("cookie-parser");
var { color } = require("./lib/color.js");

var mainrouter = require("./routes/main"),
    apirouter = require("./routes/api"),
    celengan = require("./routes/celengan")

var app = express();
app.use(cookieParser());

app.set("trust proxy", true);
app.set("json spaces", 2);
app.use(
  bodyParser.urlencoded({
    extended: true,
  })  
);
app.use(useragent.express());
app.use(expressip().getIpInfoMiddleware);
app.use(cors());
app.use(secure);
app.use(express.static("public"));

app.use("/", mainrouter);
app.use("/celengan", celengan);
app.use("/apikuv1", apirouter);

app.listen(PORT, () => {
  console.log(color("Server running on port " + PORT, "green"));
});

module.exports = app;
